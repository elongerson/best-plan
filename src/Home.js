import React from 'react'
import {useGlobalContext} from './context'
const Home = () => {
  const {closeSubmenu} = useGlobalContext();
  return (
    <section className="home" onMouseOver={closeSubmenu}>
      <div className="home-center">
        <article className="home-info">
          <h1>
            Variety of plans, <br />
            One platform
          </h1>
          <p>
            Proin consectetur sed erat eu auctor. Sed ut dui et nisi tristique sagittis eget vel velit. Maecenas et varius massa. Donec a mi purus. Sed venenatis maximus urna, vel auctor neque tincidunt sit amet. Suspendisse ut elit tempus quam efficitur maximus. In semper tincidunt suscipit.
          </p>
          <button className="btn">
            Start now
          </button>
        </article>
      </div>
    </section>
  )
}

export default Home
