import {FaUtensils, FaBriefcase, FaCar, FaLandmark, FaBuilding, FaBuffer, FaTelegramPlane, FaDigitalTachograph } from 'react-icons/fa';
import React from 'react';
const sublinks = [
  {
    page: 'careers',
    links: [
      { label: 'internship', icon: <FaBriefcase />, url: '/careers' },
      { label: 'fixed term', icon: <FaBriefcase />, url: '/careers' },
      { label: 'indeterminate term', icon: <FaBriefcase />, url: '/careers' },
      { label: 'freelance', icon: <FaBriefcase />, url: '/careers' }
    ],
  },
  {
    page: 'restaurants',
    links: [
      { label: 'chiken', icon: <FaUtensils />, url: '/restaurants' },
      { label: 'burger', icon: <FaUtensils />, url: '/restaurants' },
      { label: 'sushi', icon: <FaUtensils />, url: '/restaurants' },
      { label: 'soup', icon: <FaUtensils />, url: '/restaurants' },
      { label: 'vegetarian', icon: <FaUtensils />, url: '/restaurants' }
    ],
  },
  {
    page: 'markets',
    links: [
      { label: 'cars', icon: <FaCar />, url: '/markets' },
      { label: 'housing', icon: <FaBuilding />, url: '/markets' },
      { label: 'digital', icon: <FaDigitalTachograph />, url: '/markets' },
      { label: 'others', icon: <FaBuffer />, url: '/markets' },
    ],
  },
  {
    page: 'home',
    links: [
      { label: 'about', icon: <FaLandmark />, url: '/' },
      { label: 'contact', icon: <FaTelegramPlane />, url: '/contact' },
    ],
  },
];

export default sublinks;
