import React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navbar from './Navbar'
import Home from './Home'
import Sidebar from './Sidebar'
import Submenu from './Submenu'
import Career from './pages/Career';
import Restaurant from './pages/Restaurant';
import Market from './pages/Market';
import Contact from './pages/Contact';
import Footer from './Footer';
function App() {
  return (
    <Router>
      <Navbar />
      <Sidebar/>
      <Submenu/>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/careers">
          <Career />
        </Route>
        <Route path="/restaurants">
          <Restaurant />
        </Route>
        <Route path="/markets">
          <Market />
        </Route>
        <Route path="/contact">
          <Contact />
        </Route>
        {/* <Route path="*">
          <Error />
        </Route> */}
      </Switch>
      <Footer/>
    </Router>
  )
}

export default App
